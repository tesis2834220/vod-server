package main

import (
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"runtime/debug"

	"database/sql"

	_ "modernc.org/sqlite"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
)

const UPLOAD_FOLDER = "/uploads"

const PENDING_UPLOAD = "PENDING_UPLOAD"
const PENDING_TRANSCODE = "PENDING_TRANSCODE"
const TRANSCODING = "TRANSCODING"
const READY = "READY"

const VideosJoinQualities = `FROM videos JOIN qualities ON videos.id = qualities.video_id`
const QualitiesJoinVideos = `FROM qualities JOIN videos ON qualities.video_id = videos.id`

func GetDBConnection() *sql.DB {
	db, err := sql.Open("sqlite", os.Getenv("DB_PATH"))
	if err != nil {
		panic(err)
	}
	return db
}

func CreateVideoTable(db *sql.DB) {
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS videos (
		id TEXT PRIMARY KEY,
		name TEXT
	)`)
	if err != nil {
		panic(err)
	}
}

func CreateQualityTable(db *sql.DB) {
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS qualities (
		id TEXT PRIMARY KEY,
		resolution TEXT,
		bitrate TEXT,
		video_codec TEXT,
		audio_codec TEXT,
		profile TEXT,
		step TEXT,
		path TEXT,
		original_id TEXT,
		transcode_time TEXT,
		transcode_start TEXT,
		transcode_end TEXT,
		runner_name TEXT,
		video_id TEXT,
		FOREIGN KEY(video_id) REFERENCES videos(id)
	)`)
	if err != nil {
		panic(err)
	}
}

type Video struct {
	Id   string
	Name string
}

func InsertVideo(tx *sql.Tx, video Video) error {
	_, err := tx.Exec("INSERT INTO videos (id, name) VALUES (?, ?)", video.Id, video.Name)
	return err
}

func UpdateVideo(tx *sql.Tx, id string, video Video) error {
	_, err := tx.Exec("UPDATE videos SET name = ? WHERE id = ?", video.Name, id)
	return err
}

func RunTransaction(db *sql.DB, f func(*sql.Tx) error) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	err = f(tx)
	if err != nil {
		rollbackErr := tx.Rollback()
		if rollbackErr != nil {
			return rollbackErr
		}
		return err
	}
	return tx.Commit()
}

type Quality struct {
	Id             string
	Resolution     string
	Bitrate        string
	VideoCodec     string
	AudioCodec     string
	Step           string
	Path           string
	OriginalId     string
	TranscodeTime  string
	TranscodeStart string
	TranscodeEnd   string
	RunnerName     string
	VideoId        string
	Profile        string
}

func (q Quality) MarshalMap() map[string]string {
	return map[string]string{
		"id":              q.Id,
		"resolution":      q.Resolution,
		"bitrate":         q.Bitrate,
		"video_codec":     q.VideoCodec,
		"audio_codec":     q.AudioCodec,
		"step":            q.Step,
		"path":            q.Path,
		"original_id":     q.OriginalId,
		"transcode_time":  q.TranscodeTime,
		"transcode_start": q.TranscodeStart,
		"transcode_end":   q.TranscodeEnd,
		"runner_name":     q.RunnerName,
		"video_id":        q.VideoId,
		"profile":         q.Profile,
	}
}

func InsertQuality(tx *sql.Tx, quality Quality) error {
	_, err := tx.Exec("INSERT INTO qualities (id, resolution, bitrate, video_codec, audio_codec, profile, step, path, original_id, transcode_time, transcode_start, transcode_end, runner_name, video_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", quality.Id, quality.Resolution, quality.Bitrate, quality.VideoCodec, quality.AudioCodec, quality.Profile, quality.Step, quality.Path, quality.OriginalId, quality.TranscodeTime, quality.TranscodeStart, quality.TranscodeEnd, quality.RunnerName, quality.VideoId)
	return err
}

func SelectQualityById(db *sql.DB, id string) (Quality, error) {
	var quality Quality
	err := db.QueryRow("SELECT id, resolution, bitrate, video_codec, audio_codec, profile, step, path, original_id, transcode_time,transcode_start, transcode_end, runner_name, video_id FROM qualities WHERE id = ?", id).Scan(&quality.Id, &quality.Resolution, &quality.Bitrate, &quality.VideoCodec, &quality.AudioCodec, &quality.Profile, &quality.Step, &quality.Path, &quality.OriginalId, &quality.TranscodeTime, &quality.TranscodeStart, &quality.TranscodeEnd, &quality.RunnerName, &quality.VideoId)
	if err != nil {
		return Quality{}, err
	}
	return quality, nil
}

func UpdateQualityStatus(tx *sql.Tx, id string, state string) error {
	_, err := tx.Exec("UPDATE qualities SET step = ? WHERE id = ?", state, id)
	return err
}

func UpdateQualityTranscodeTime(tx *sql.Tx, id string, transcodeTime, transcodeStart, transcodeEnd string, runnerName string) error {
	_, err := tx.Exec("UPDATE qualities SET transcode_time = ?, transcode_start = ?, transcode_end = ?, runner_name = ? WHERE id = ?", transcodeTime, transcodeStart, transcodeEnd, runnerName, id)
	return err
}

func GetQualityPath(db *sql.DB, id string) (string, error) {
	var path string
	err := db.QueryRow("SELECT path FROM qualities WHERE id = ?", id).Scan(&path)
	if err != nil {
		return "", err
	}
	return path, nil
}

func CreateVideo(tx *sql.Tx, video Video, qualities []Quality) error {
	videoError := InsertVideo(tx, video)
	if videoError != nil {
		fmt.Println(videoError)
		fmt.Println(video)
		return videoError
	}
	for _, quality := range qualities {
		quality.VideoId = video.Id
		qualityError := InsertQuality(tx, quality)
		if qualityError != nil {
			fmt.Println(qualityError)
			fmt.Println(quality)
			return qualityError
		}
	}
	return nil
}

func GetRedisConnection() *redis.Client {

	return redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:6379", os.Getenv("REDIS_IP")),
		Password: "",
		DB:       0,
	})
}

func jsonError(w http.ResponseWriter, err string) error {
	return json.NewEncoder(w).Encode(map[string]string{"error": err})
}

func InternalError(w http.ResponseWriter) {
	w.WriteHeader(http.StatusInternalServerError)
	jsonError(w, "internal error")
}

func qualityPath(qualityId string) string {
	return fmt.Sprintf("%s/%s", UPLOAD_FOLDER, qualityId)
}

func videos_post(w http.ResponseWriter, req *http.Request) {
	fmt.Printf("Processing videos_post\n")
	name := req.FormValue("name")
	if name == "" {
		w.WriteHeader(http.StatusBadRequest)
		jsonError(w, "name is required")
		return
	}

	f, _, err := req.FormFile("video")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonError(w, "video is required")
		return
	}
	defer f.Close()

	video := Video{
		Id:   uuid.New().String(),
		Name: name,
	}
	fmt.Printf("Form parsed %s\n", video.Name)
	originalQualityId := uuid.New().String()
	originalQualityFilePath := qualityPath(originalQualityId)

	originalQuality := Quality{
		Id:             originalQualityId,
		Resolution:     "original",
		Bitrate:        "original",
		VideoCodec:     "original",
		AudioCodec:     "original",
		Profile:        "original",
		Step:           PENDING_UPLOAD,
		Path:           originalQualityFilePath,
		OriginalId:     "",
		TranscodeTime:  "",
		TranscodeStart: "",
		TranscodeEnd:   "",
		RunnerName:     "",
	}
	// https://doc.cdnvideo.ru/en/Services_setup/recommended_stream_params/
	qualities := []Quality{
		{
			Id:             uuid.New().String(),
			Resolution:     "1920:1080",
			Bitrate:        "4M",
			VideoCodec:     "libx264",
			AudioCodec:     "aac",
			Profile:        "high",
			Step:           PENDING_UPLOAD,
			Path:           "",
			OriginalId:     originalQuality.Id,
			TranscodeTime:  "",
			TranscodeStart: "",
			TranscodeEnd:   "",
			RunnerName:     "",
		},
		{
			Id:             uuid.New().String(),
			Resolution:     "1280:720",
			Bitrate:        "2.5M",
			VideoCodec:     "libx264",
			AudioCodec:     "aac",
			Profile:        "main",
			Step:           PENDING_UPLOAD,
			Path:           "",
			OriginalId:     originalQuality.Id,
			TranscodeTime:  "",
			TranscodeStart: "",
			TranscodeEnd:   "",
			RunnerName:     "",
		},
		{
			Id:             uuid.New().String(),
			Resolution:     "854:480",
			Bitrate:        "1.5M",
			VideoCodec:     "libx264",
			AudioCodec:     "aac",
			Profile:        "main",
			Step:           PENDING_UPLOAD,
			Path:           "",
			OriginalId:     originalQuality.Id,
			TranscodeTime:  "",
			TranscodeStart: "",
			TranscodeEnd:   "",
			RunnerName:     "",
		},
	}

	allQualities := append([]Quality{originalQuality}, qualities...)

	db := GetDBConnection()
	err = RunTransaction(db, func(tx *sql.Tx) error {
		fmt.Printf("CreateVideo %s\n", video.Id)
		return CreateVideo(tx, video, allQualities)
	})
	if err != nil {
		panic(err)
	}
	fmt.Printf("Saved video file %s\n", originalQualityFilePath)
	err = saveVideoFile(f, originalQuality.Path)
	if err != nil {
		panic(err)
	}

	for _, q := range allQualities {
		err = RunTransaction(db, func(tx *sql.Tx) error {
			return UpdateQualityStatus(tx, q.Id, PENDING_TRANSCODE)
		})
		if err != nil {
			panic(err)
		}
	}

	response := map[string]interface{}{
		"id":        video.Id,
		"name":      video.Name,
		"qualities": []map[string]string{},
	}

	for _, q := range qualities {
		response["qualities"] = append(response["qualities"].([]map[string]string), q.MarshalMap())
	}

	json.NewEncoder(w).Encode(response)

}

func sendQualityTask(w http.ResponseWriter, req *http.Request) {
	fmt.Printf("Runing sendQualityTask\n")
	qualityId := chi.URLParam(req, "id")
	if qualityId == "" {
		fmt.Println("qualityId is required")
		w.WriteHeader(http.StatusBadRequest)
		jsonError(w, "id is required")
		return
	}

	quality, err := SelectQualityById(GetDBConnection(), qualityId)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusNotFound)
		jsonError(w, "quality not found")
		return
	}

	if quality.Step != PENDING_TRANSCODE {
		fmt.Println("invalid quality state: " + quality.Step)
		w.WriteHeader(http.StatusBadRequest)
		jsonError(w, "invalid quality state: "+quality.Step)
		return
	}

	ctx := req.Context()
	client := GetRedisConnection()
	defer client.Close()
	task := quality.MarshalMap()

	hsetError := client.HSet(ctx, quality.Id, task).Err()
	if hsetError != nil {
		fmt.Println(hsetError)
		panic(hsetError)
	}

	lpushError := client.LPush(ctx, "transcode_tasks", quality.Id).Err()
	if lpushError != nil {
		fmt.Println(lpushError)
		panic(lpushError)
	}
	fmt.Printf("Pushed task, quality: %s -> original: %s\n", task["id"], task["original_id"])

}

func createUploadFolder() error {
	err := os.MkdirAll(UPLOAD_FOLDER, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

func saveVideoFile(file multipart.File, path string) error {

	dst, err := os.Create(path)
	if err != nil {
		return err
	}
	defer dst.Close()
	_, err = io.Copy(dst, file)
	if err != nil {
		return err
	}
	return nil
}

func uploadQualityFile(w http.ResponseWriter, req *http.Request) {
	fmt.Printf("Runing uploadQualityFile\n")
	qualityId := chi.URLParam(req, "id")
	if qualityId == "" {
		fmt.Printf("qualityId is required\n")
		w.WriteHeader(http.StatusBadRequest)
		jsonError(w, "id is required")
		return
	}
	file, fh, err := req.FormFile("video")

	if err != nil {
		fmt.Println(err)
		debug.PrintStack()
		w.WriteHeader(http.StatusBadRequest)
		jsonError(w, "video is required")
		return
	}
	defer file.Close()
	if fh.Size <= 0 {
		fmt.Println(err)
		debug.PrintStack()
		w.WriteHeader(http.StatusBadRequest)
		jsonError(w, "video is empty")
		return
	}

	transcodeTime := req.FormValue("transcode_time")
	transcodeStart := req.FormValue("transcode_start")
	transcodeEnd := req.FormValue("transcode_end")
	runnerName := req.FormValue("runner_name")
	err = saveVideoFile(file, qualityPath(qualityId))
	if err != nil {
		fmt.Println(err)
		debug.PrintStack()
		w.WriteHeader(http.StatusInternalServerError)
		jsonError(w, "failed to save video file")
		return
	}

	db := GetDBConnection()
	err = RunTransaction(db, func(tx *sql.Tx) error {
		updateErr := UpdateQualityStatus(tx, qualityId, READY)
		if updateErr != nil {
			return updateErr
		}
		return UpdateQualityTranscodeTime(tx, qualityId, transcodeTime, transcodeStart, transcodeEnd, runnerName)
	})
	if err != nil {
		panic(err)
	}

	fmt.Printf("Quality updated %s %s %d %s\n", qualityId, transcodeTime, fh.Size, runnerName)
}

func downloadQualityFile(w http.ResponseWriter, req *http.Request) {
	fmt.Printf("Runing downloadQualityFile\n")
	qualityId := chi.URLParam(req, "id")
	if qualityId == "" {
		w.WriteHeader(http.StatusBadRequest)
		jsonError(w, "id is required")
		return
	}

	db := GetDBConnection()
	qualityPath, err := GetQualityPath(db, qualityId)
	if qualityPath == "" || err != nil {
		w.WriteHeader(http.StatusNotFound)
		jsonError(w, "quality not found")
		return
	}
	http.ServeFile(w, req, qualityPath)
}

func main() {

	err := createUploadFolder()
	if err != nil {
		panic(err)
	}

	if len(os.Args) < 3 {
		fmt.Println("Usage: server <db_path>")
		os.Exit(1)
	}

	err = os.Setenv("DB_PATH", os.Args[1])
	if err != nil {
		panic(err)
	}
	err = os.Setenv("REDIS_IP", os.Args[2])
	if err != nil {
		panic(err)
	}

	db := GetDBConnection()

	CreateVideoTable(db)
	CreateQualityTable(db)

	router := chi.NewRouter()
	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)

	router.Post("/videos", videos_post)
	router.Post("/qualities/{id}", uploadQualityFile)
	router.Get("/qualities/{id}", downloadQualityFile)
	router.Put("/transcode/{id}", sendQualityTask)
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	fmt.Println("Listening on :80...")
	http.ListenAndServe(":80", router)
}
